# lua-rs

Lua port written in Rust.

[![build status](https://gitlab.com/ahmedcharles/lua-rs/badges/master/pipeline.svg)
](https://gitlab.com/ahmedcharles/lua-rs/tree/master)
[![Crates.io](https://img.shields.io/crates/v/lua-rs.svg)
](https://crates.io/crates/lua-rs)

## Usage

Add this to your `Cargo.toml` (crates.io):

```toml
[dependencies]
lua-rs = "0.0.11"
```

or (git):

```toml
[dependencies]
lua-rs = { git = "https://gitlab.com/ahmedcharles/lua-rs" }
```

Add this to your crate root:

```rust
extern crate lua_rs;
```

Get the latest version from [GitLab](https://gitlab.com/ahmedcharles/lua-rs).

## License
```
Copyright (C) 2015-2020 Ahmed Charles - acharles@outlook.com
Distributed under the MIT License.
   (See accompanying file LICENSE.txt or copy at
         http://opensource.org/licenses/MIT)
```

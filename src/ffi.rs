// Copyright (C) 2019-2020 Ahmed Charles - acharles@outlook.com
// Distributed under the MIT License.
//    (See accompanying file LICENSE.txt or copy at
//          http://opensource.org/licenses/MIT)

//! FFI interface for Lua C bindings.

#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

#![allow(clippy::missing_safety_doc)]

use core as std;

#[cfg(debug_assertions)]
const LUAI_MAXSTACK: libc::c_int = 50_000;
#[cfg(not(debug_assertions))]
const LUAI_MAXSTACK: libc::c_int = 1_000_000;

// lua.h

pub const LUA_MULTRET: libc::c_int = -1;

pub const LUA_REGISTRYINDEX: libc::c_int = -LUAI_MAXSTACK - 1000;

pub const LUA_OK: libc::c_int = 0;
pub const LUA_ERRSYNTAX: libc::c_int = 3;

pub type lua_State = libc::c_void;

pub const LUA_TSTRING: libc::c_int = 4;
pub const LUA_TTABLE: libc::c_int = 5;

pub const LUA_NUMTAGS: libc::c_int = 9;

pub const LUA_MINSTACK: libc::c_int = 20;

pub type lua_Number = f64;

pub type lua_KContext = libc::ptrdiff_t;

pub type lua_CFunction = Option<unsafe extern "C" fn(l: *mut lua_State) -> libc::c_int>;

pub type lua_KFunction = Option<
    unsafe extern "C" fn(l: *mut lua_State, status: libc::c_int, ctx: lua_KContext) -> libc::c_int,
>;

pub type lua_Alloc = Option<
    unsafe extern "C" fn(
        ud: *mut libc::c_void,
        ptr: *mut libc::c_void,
        osize: libc::size_t,
        nsize: libc::size_t,
    ) -> *mut libc::c_void,
>;

extern "C" {
    pub fn lua_newstate(l: lua_Alloc, ud: *mut libc::c_void) -> *mut lua_State;
    pub fn lua_close(l: *mut lua_State);

    pub fn lua_gettop(l: *mut lua_State) -> libc::c_int;
    pub fn lua_settop(l: *mut lua_State, idx: libc::c_int);
    pub fn lua_rotate(l: *mut lua_State, idx: libc::c_int, n: libc::c_int);

    pub fn lua_type(l: *mut lua_State, idx: libc::c_int) -> libc::c_int;
    pub fn lua_typename(l: *mut lua_State, tp: libc::c_int) -> *const libc::c_char;

    pub fn lua_toboolean(l: *mut lua_State, idx: libc::c_int) -> libc::c_int;
    pub fn lua_tolstring(
        l: *mut lua_State,
        idx: libc::c_int,
        len: *mut libc::size_t,
    ) -> *const libc::c_char;

    pub fn lua_pushstring(l: *mut lua_State, s: *const libc::c_char) -> *const libc::c_char;
    pub fn lua_pushlstring(
        l: *mut lua_State,
        s: *const libc::c_char,
        len: libc::size_t,
    ) -> *const libc::c_char;
    pub fn lua_pushfstring(l: *mut lua_State, fmt: *const libc::c_char, ...)
        -> *const libc::c_char;
    pub fn lua_pushcclosure(l: *mut lua_State, f: lua_CFunction, n: libc::c_int);
    pub fn lua_pushboolean(l: *mut lua_State, b: libc::c_int);

    pub fn lua_getglobal(l: *mut lua_State, name: *const libc::c_char) -> libc::c_int;
    pub fn lua_rawgeti(l: *mut lua_State, idx: libc::c_int, n: libc::c_longlong) -> libc::c_int;

    pub fn lua_createtable(l: *mut lua_State, narr: libc::c_int, nrec: libc::c_int);

    pub fn lua_setglobal(l: *mut lua_State, name: *const libc::c_char);
    pub fn lua_setfield(l: *mut lua_State, idx: libc::c_int, k: *const libc::c_char);
    pub fn lua_rawseti(l: *mut lua_State, idx: libc::c_int, n: libc::c_longlong);

    pub fn lua_pcallk(
        l: *mut lua_State,
        nargs: libc::c_int,
        nresults: libc::c_int,
        errfunc: libc::c_int,
        ctx: lua_KContext,
        k: lua_KFunction,
    ) -> libc::c_int;
}

#[inline(always)]
pub unsafe fn lua_pcall(
    l: *mut lua_State,
    n: libc::c_int,
    r: libc::c_int,
    f: libc::c_int,
) -> libc::c_int {
    lua_pcallk(l, n, r, f, 0, None)
}

pub const LUA_GCGEN: libc::c_int = 10;

extern "C" {
    pub fn lua_gc(l: *mut lua_State, what: libc::c_int, ...) -> libc::c_int;

    pub fn lua_concat(l: *mut lua_State, n: libc::c_int);
}

#[inline(always)]
pub unsafe fn lua_pop(l: *mut lua_State, n: libc::c_int) {
    lua_settop(l, -n - 1);
}

#[inline(always)]
pub unsafe fn lua_pushcfunction(l: *mut lua_State, f: lua_CFunction) {
    lua_pushcclosure(l, f, 0);
}

#[inline(always)]
pub unsafe fn lua_pushliteral(l: *mut lua_State, s: &[u8]) -> *const libc::c_char {
    lua_pushlstring(l, s.as_ptr() as _, s.len())
}

#[inline(always)]
pub unsafe fn lua_tostring(l: *mut lua_State, idx: i32) -> *const libc::c_char {
    lua_tolstring(l, idx, std::ptr::null_mut())
}

#[inline(always)]
pub unsafe fn lua_insert(l: *mut lua_State, idx: libc::c_int) {
    lua_rotate(l, idx, 1);
}

#[inline(always)]
pub unsafe fn lua_remove(l: *mut lua_State, idx: libc::c_int) {
    lua_rotate(l, idx, -1);
    lua_pop(l, 1);
}

pub const LUA_MASKCALL: libc::c_int = 1;
pub const LUA_MASKRET: libc::c_int = 1 << 1;
pub const LUA_MASKCOUNT: libc::c_int = 1 << 3;

pub type lua_Hook = Option<unsafe extern "C" fn(l: *mut lua_State, ar: *mut lua_Debug)>;

extern "C" {
    pub fn lua_sethook(l: *mut lua_State, func: lua_Hook, mask: libc::c_int, count: libc::c_int);
}

#[allow(missing_copy_implementations)]
#[allow(missing_debug_implementations)]
#[repr(C)]
pub struct lua_Debug {
    event: libc::c_int,
    name: *const libc::c_char,
    namewhat: *const libc::c_char,
    what: *const libc::c_char,
    source: *const libc::c_char,
    currentline: libc::c_int,
    linedefined: libc::c_int,
    lastlinedefined: libc::c_int,
    nups: libc::c_uchar,
    nparams: libc::c_uchar,
    isvararg: libc::c_char,
    istailcall: libc::c_char,
    ftransfer: libc::c_ushort,
    ntransfer: libc::c_ushort,
    short_src: [libc::c_char; 60],
    /* private part */
    i_ci: *mut libc::c_void,
}

// lauxlib.h

extern "C" {
    pub fn luaL_callmeta(
        l: *mut lua_State,
        obj: libc::c_int,
        e: *const libc::c_char,
    ) -> libc::c_int;
    pub fn luaL_checkstack(l: *mut lua_State, sz: libc::c_int, msg: *const libc::c_char);
    pub fn luaL_error(l: *mut lua_State, fmt: *const libc::c_char, ...) -> !;
    pub fn luaL_loadfilex(
        l: *mut lua_State,
        filename: *const libc::c_char,
        mode: *const libc::c_char,
    ) -> libc::c_int;
    pub fn luaL_loadbufferx(
        l: *mut lua_State,
        buff: *const libc::c_char,
        sz: libc::size_t,
        name: *const libc::c_char,
        mode: *const libc::c_char,
    ) -> libc::c_int;
    pub fn luaL_newstate() -> *mut lua_State;
    pub fn luaL_len(l: *mut lua_State, idx: libc::c_int) -> libc::c_longlong;
    pub fn luaL_traceback(
        l: *mut lua_State,
        l1: *mut lua_State,
        msg: *const libc::c_char,
        level: libc::c_int,
    );
    pub fn luaL_requiref(
        l: *mut lua_State,
        modname: *const libc::c_char,
        openf: lua_CFunction,
        glb: libc::c_int,
    );
}

#[inline(always)]
pub unsafe fn luaL_loadfile(l: *mut lua_State, f: *const i8) -> i32 {
    luaL_loadfilex(l, f, std::ptr::null())
}

#[inline(always)]
pub unsafe fn luaL_typename(l: *mut lua_State, idx: libc::c_int) -> *const libc::c_char {
    lua_typename(l, lua_type(l, idx))
}

#[inline(always)]
pub unsafe fn luaL_loadbuffer(
    L: *mut lua_State,
    s: *const i8,
    sz: libc::size_t,
    n: *const libc::c_char,
) -> i32 {
    luaL_loadbufferx(L, s, sz, n, std::ptr::null_mut())
}

// lualib.h

extern "C" {
    pub fn luaL_openlibs(l: *mut lua_State);
}

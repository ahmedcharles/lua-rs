// Copyright (C) 2018-2020 Ahmed Charles - acharles@outlook.com
// Distributed under the MIT License.
//    (See accompanying file LICENSE.txt or copy at
//          http://opensource.org/licenses/MIT)

//! Build script for lua-rs.

#![deny(warnings)]
#![deny(absolute_paths_not_starting_with_crate)]
#![deny(anonymous_parameters)]
#![deny(deprecated_in_future)]
#![deny(elided_lifetimes_in_paths)]
#![deny(explicit_outlives_requirements)]
#![deny(indirect_structural_match)]
#![deny(keyword_idents)]
#![deny(macro_use_extern_crate)]
#![deny(meta_variable_misuse)]
#![deny(missing_copy_implementations)]
#![deny(missing_debug_implementations)]
#![deny(missing_docs)]
#![deny(missing_doc_code_examples)]
#![deny(non_ascii_idents)]
#![deny(private_doc_tests)]
#![deny(single_use_lifetimes)]
#![deny(trivial_casts)]
#![deny(trivial_numeric_casts)]
#![deny(unreachable_pub)]
#![deny(unused_extern_crates)]
#![deny(unused_import_braces)]
#![deny(unused_labels)]
#![deny(unused_lifetimes)]
#![deny(unused_qualifications)]
#![deny(unused_results)]
#![deny(variant_size_differences)]

use cc::Build;

#[cfg(target_os = "macos")]
fn os_build(build: &mut Build) -> &mut Build {
    build.define("LUA_USE_MACOSX", None)
}

#[cfg(target_os = "linux")]
fn os_build(build: &mut Build) -> &mut Build {
    build.define("LUA_USE_LINUX", None)
}

#[cfg(not(any(target_os = "macos", target_os = "linux")))]
fn os_build(build: &mut Build) -> &mut Build {
    build
}

fn main() {
    let out_dir_s = std::env::var_os("OUT_DIR").unwrap();
    let out_dir = std::path::Path::new(&out_dir_s);
    let debug = std::env::var("PROFILE").unwrap() == "debug";

    let mut build = Build::new();
    let _ = build.include("puc-lua/src");
    let _ = os_build(&mut build);
    if debug {
        let _ = build.define("LUA_USER_H", Some("\"ltests.h\""));
    }
    let cc = build.get_compiler();
    let mut luac_c = cc.to_command();

    if debug {
        let _ = build.file("puc-lua/src/ltests.c");
    }
    let _ = build.file("puc-lua/src/lapi.c");
    let _ = build.file("puc-lua/src/lcode.c");
    let _ = build.file("puc-lua/src/lctype.c");
    let _ = build.file("puc-lua/src/ldebug.c");
    let _ = build.file("puc-lua/src/ldo.c");
    let _ = build.file("puc-lua/src/ldump.c");
    let _ = build.file("puc-lua/src/lfunc.c");
    let _ = build.file("puc-lua/src/lgc.c");
    let _ = build.file("puc-lua/src/llex.c");
    let _ = build.file("puc-lua/src/lmem.c");
    let _ = build.file("puc-lua/src/lobject.c");
    let _ = build.file("puc-lua/src/lopcodes.c");
    let _ = build.file("puc-lua/src/lparser.c");
    let _ = build.file("puc-lua/src/lstate.c");
    let _ = build.file("puc-lua/src/lstring.c");
    let _ = build.file("puc-lua/src/ltable.c");
    let _ = build.file("puc-lua/src/ltm.c");
    let _ = build.file("puc-lua/src/lundump.c");
    let _ = build.file("puc-lua/src/lvm.c");
    let _ = build.file("puc-lua/src/lzio.c");

    let _ = build.file("puc-lua/src/lauxlib.c");

    let _ = build.file("puc-lua/src/linit.c");
    let _ = build.file("puc-lua/src/lbaselib.c");
    let _ = build.file("puc-lua/src/lcorolib.c");
    let _ = build.file("puc-lua/src/ldblib.c");
    let _ = build.file("puc-lua/src/liolib.c");
    let _ = build.file("puc-lua/src/lmathlib.c");
    let _ = build.file("puc-lua/src/loadlib.c");
    let _ = build.file("puc-lua/src/loslib.c");
    let _ = build.file("puc-lua/src/lstrlib.c");
    let _ = build.file("puc-lua/src/ltablib.c");
    let _ = build.file("puc-lua/src/lutf8lib.c");

    build.compile("lua");

    let _ = luac_c.arg("puc-lua/src/luac.c");
    if cc.is_like_gnu() || cc.is_like_clang() {
        let _ = luac_c
            .arg(out_dir.join("liblua.a"))
            .arg("-o")
            .arg(out_dir.join("luac"))
            .arg("-lm");
    } else if cc.is_like_msvc() {
        let _ = luac_c
            .arg(out_dir.join("liblua.a"))
            .arg(format!("/OUT:{}", out_dir.join("luac.exe").display()));
    }
    assert!(luac_c.status().unwrap().success());

    for entry in std::fs::read_dir("puc-lua/src").unwrap() {
        println!("cargo:rerun-if-changed={}", entry.unwrap().path().display());
    }
}

// Copyright (C) 2018-2019 Ahmed Charles - acharles@outlook.com
// Distributed under the MIT License.
//    (See accompanying file LICENSE.txt or copy at
//          http://opensource.org/licenses/MIT)

#![deny(warnings)]
#![deny(absolute_paths_not_starting_with_crate)]
#![deny(anonymous_parameters)]
#![deny(deprecated_in_future)]
#![deny(elided_lifetimes_in_paths)]
#![deny(explicit_outlives_requirements)]
#![deny(indirect_structural_match)]
#![deny(keyword_idents)]
#![deny(macro_use_extern_crate)]
#![deny(meta_variable_misuse)]
#![deny(missing_copy_implementations)]
#![deny(missing_debug_implementations)]
#![deny(missing_docs)]
#![deny(missing_doc_code_examples)]
#![deny(non_ascii_idents)]
#![deny(private_doc_tests)]
#![deny(single_use_lifetimes)]
#![deny(trivial_casts)]
#![deny(trivial_numeric_casts)]
#![deny(unreachable_pub)]
#![deny(unused_extern_crates)]
#![deny(unused_import_braces)]
#![deny(unused_labels)]
#![deny(unused_lifetimes)]
#![deny(unused_qualifications)]
#![deny(unused_results)]
#![deny(variant_size_differences)]

use std::env;
use std::fs;
use std::io::ErrorKind;
use std::process::Command;

#[cfg(target_os = "linux")]
fn os(cmd: &mut Command) -> &mut Command {
    cmd.arg("-e").arg("_noninteractive=true")
}

#[cfg(not(target_os = "linux"))]
fn os(cmd: &mut Command) -> &mut Command {
    cmd.arg("-e").arg("_port=true")
}

#[test]
fn run_tests() {
    fs::create_dir_all("tests/libs/P1").unwrap();

    let lua = env::current_exe()
        .map(|mut path| {
            let _ = path.pop();
            if path.ends_with("deps") {
                let _ = path.pop();
            }
            path.push("lua");
            path
        })
        .unwrap();
    let mut cmd = Command::new(lua);
    let output = os(&mut cmd)
        .arg("all.lua")
        .current_dir("tests")
        .output()
        .unwrap();

    match fs::remove_file("tests/time-debug.txt") {
        Ok(()) => (),
        Err(ref e) if e.kind() == ErrorKind::NotFound => (),
        Err(e) => Err(e).unwrap(),
    }
    match fs::remove_file("tests/time.txt") {
        Ok(()) => (),
        Err(ref e) if e.kind() == ErrorKind::NotFound => (),
        Err(e) => Err(e).unwrap(),
    }

    println!("status: {}", output.status);
    println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
    println!("stderr: {}", String::from_utf8_lossy(&output.stderr));

    assert!(output.status.success());
}
